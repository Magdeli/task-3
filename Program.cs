﻿using System;

namespace Task_3
{
    class Program
    {
        static void Main(string[] args)
        {
            #region \\Variables

            int n;
            string square;
            string space;

            #endregion


            #region \\Getting the size of the square

            do
            {
                Console.WriteLine("Please enter a number for how large you want the square:");

                n = int.Parse(Console.ReadLine());

            } while (n % 1 != 0);

            #endregion

            #region \\Making the square

            for(int i = 0; i < n; i++)
            {
                if (i == 0|| i == (n-1))
                {
                    square = new String('#', n);
                    Console.WriteLine(square);
                }
                else
                {
                    square = "#";
                    space = new string(' ', (n-2));
                    Console.WriteLine(square + space + square);
                }
            }

            #endregion
        }
    }
}
